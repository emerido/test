<?php include __DIR__ .  '/../app/bootstrap.php';


$app = new \Smart\Web\App();


$ser = $app->getService();


$ser['poll'] = $ser->share(function($c) {
    return new Poll($c);
});
$ser['poll_answer'] = $ser->share(function($c) {
    return new PollAnswer($c);
});
$ser['quest'] = $ser->share(function($c) {
    return new Question($c);
});
$ser['quest_answer'] = $ser->share(function($c) {
    return new QuestionAnswer($c);
});


// Configure application
$app->config->set([
    'db'    => [
        'dsn' => 'mysql:host=localhost;port=3306;dbname=test',
//        'user'  => 'root'
    ],
    'view' => [
        'extension' => '.php',
        'directory' => __DIR__ . '/../app/views/',
    ]
]);


// Set default view variables
$app->view->set([
    'title' => 'Test Application',
    'content' => null,
]);


$app->get('/', function () use($app) {
    return $app->view->render('index');
});

$app->get('/poll/{id}', function ($id) use($app) {

    $poll = $app->poll->findOne($id);

    $poll['quests'] = $app->quest->findByPoll($id);

    // Get answers
    array_walk($poll['quests'], function(&$item, $key) use ($app) {
        $item['answers'] = $app->quest_answer->findByQuestion($item['id']);
    });

    $app->view->set('poll', $poll);

    return $app->view
        ->layout('index')
        ->render('index/view');
});


$app->get('/admin/', function () use($app) {
    return $app->view
        ->layout('admin')
        ->render('admin/list');
});


$app->get('/admin/edit(/{id})', function ($id = null) use ($app) {
    if ($id) $app->view->set('id', $id);
    return $app->view
        ->layout('admin')
        ->render('admin/form');
});

$app->get('/admin/view/{id}', function ($id = null) use ($app) {
    if ($id) $app->view->set('id', $id);
    return $app->view
        ->layout('admin')
        ->render('admin/form');
});

include '../app/api/poll.php';
include '../app/api/quest.php';
include '../app/api/answer.php';


// * Handle 500 error
$app->error(function(\Smart\Web\App $app, \Exception $e) {
    $app->response->setCode(500);
    $app->response->setContent($e->getMessage());
    if ($app->request->isAjax()) {
        $app->response->setJsonContent(['message' => $e->getMessage()]);
    }
});


// * Handle 404 error
$app->error(404, function(\Smart\Web\App $app, \Exception $e) {
    $app->response->setCode(404);
    $app->response->setContent($e->getMessage());
    if ($app->request->isAjax()) {
        $app->response->setJsonContent(['message' => $e->getMessage()]);
    }
});


// * Run application
echo $app->run()->getContent();