/**
 *
 */
angular.module('admin', ['myUtils', 'myModels']);


var Poll = {};



Poll.ListCtrl = function ($scope, $filter, Poll) {


    $scope.ready = false;

    $scope.polls = [];
    $scope.pollsAll = [];


    $scope.delete = function($poll) {
        $scope.polls.splice($scope.polls.indexOf($poll), 1);
        $poll.$delete();
    };


    $scope.toggle = function($poll) {
        $poll.status = $poll.status == 0 ? 1 : 0;
        $poll.$save();
    };


    $scope.filter = function(type) {
        if (type == 3) {
            $scope.polls = $scope.pollsAll;
        } else {
            $scope.polls = $.map($scope.pollsAll, function(poll) {
                if (poll.status == type) {
                    return poll;
                }
            });
        }
    };

    // Load polls
    Poll.query(function(items) {
        $scope.ready = true;
        $scope.polls = items;
        $scope.pollsAll = items;
    });

};



Poll.FormCtrl = function ($scope, Poll, Quest, Answer) {

    // Current poll id
    $scope.id = null;

    // Default poll
    $scope.poll = null;


    $scope.ready = false;


    // Initialize poll
    $scope.$watch('id', function(id) {

        if (!isNaN(id) && id) {
            $scope.poll = Poll.full({'id': id}, function(data) {
                $scope.init(data);
            });
        } else {
            $scope.init(new Poll({
                'status' : 0,
                'quests' : []
            }));
        }
        $scope.ready = true;

    });


    $scope.init = function($poll) {
        if ($poll.quests == null)
            $poll.quests = [];

        if ($poll.quests.length > 0) {
            $.map($poll.quests, function(quest, k) {
                $poll.quests[k] = new Quest(quest);
            });
            if ($poll.quests.length < 2) {
                $scope.addQuest($poll);
            }
        } else {
            $scope.addQuest($poll);
            $scope.addQuest($poll);
        }

        $.map($poll.quests, function(quest, k) {
            if (quest.answers == null)
                quest.answers = [];

            if (quest.answers.length > 0) {
                $.map(quest.answers, function(answer, i) {
                    quest.answers[i] = new Answer(answer);
                });
            } else {
                $scope.addAnswer(quest);
                $scope.addAnswer(quest);
            }
            $poll.quests[k] = quest;
        });

        $scope.poll = $poll;
    };


    $scope.addQuest = function($poll, data) {
        data = data || {};
        data.required = !!data.required;
        data.multiply = !!data.multiply;
        data = new Quest(data);

        $scope.addAnswer(data);
        $scope.addAnswer(data);

        $poll.quests.push(data);
    };


    $scope.addAnswer = function($quest, data) {
        if ($quest.answers == null)
            $quest.answers = [];

        $quest.answers.push(new Answer());
    };


    $scope.deleteQuest = function($quest) {
        if ($quest.id) {
            $quest.$delete();
        }
        $scope.poll.quests.splice($scope.poll.quests.indexOf($quest), 1);

        if ($scope.poll.quests.length < 2) {
            $scope.addQuest($scope.poll);
        }
    };


    $scope.deleteAnswer = function($answer) {
        if ($answer.id) {
            $answer.$delete();
        }

        $.map($scope.poll.quests, function(quest) {
            var i = quest.answers.indexOf($answer);
            if (i > -1) {
                quest.answers.splice(i, 1);
                if (quest.answers.length < 2) {
                    $scope.addAnswer(quest);
                }
            }
        });
    };



    $scope.submit = function(form) {

        if (form.$valid) {
            if ($scope.poll.quests.length < 2) {
                form.$valid = false;
            }
            $.map($scope.poll.quests, function (quest) {
                $.map(quest.answers, function (answer, i) {
                    if (!answer.text && i < 2) {
                        console.log(answer, i);
                        form.$valid = false;
                        //                    quest.answers.splice(i, 1);
                    }
                });

                if (quest.answers.length < 2) {
                    form.$valid = false;
                }
            });
            $scope.poll.$edit();
            form.$valid = true;
        }
    };
};