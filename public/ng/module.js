/**
 * Utils module
 */
angular.module('myUtils', [])
    .directive('myStatus', function() {
        return {
            template : '<span class="label" ng-class="statusClass">{{ statusName }}</span>',
            scope : true,
            link : function($scope, $element, $attr) {
                update($scope.$eval($attr['myStatus']));
                $scope.$watch($attr['myStatus'], update);
                function update(value) {
                    if (value == 0) {
                        $scope.statusName = 'Черновик';
                        $scope.statusClass = 'label-inverse';
                    } else if (value == 1) {
                        $scope.statusName = 'Активный';
                        $scope.statusClass = 'label-info'
                    } else if (value == 2) {
                        $scope.statusName = 'Завершен';
                        $scope.statusClass = 'label-success';
                    }
                }
            }
        };
    }).directive('myAnswer', function() {
        return function ($scope, $element, $attr) {
            $scope.$watch('answer.text', function (value) {
                if ($scope.$last && value) {
                    $scope.addAnswer($scope.quest);
                }
            });
        }
    }).directive('myProgress', function() {
        return function ($scope, $element, $attr) {


            var count = 0,
                width;

            $.map($scope.quest.results, function(checked) {
                if (checked.answer_id == $scope.answer.id) {
                    count++;
                }
            });

            width = (count / $scope.quest.results.length * 100).toFixed(2);

            $scope.answer.count = count;

            if (width > 0) {
                $element.text(width + '%').css('width', width + '%');
            }
        }
    }).filter('type', function() {
        return function (value) {
            if (value == 1) {
                return 'checkbox';
            }
            return 'radio';
        }
    })
;

/**
 * Rest models resources
 */
angular.module('myModels', ['ngResource'])
    .factory('Poll', function($resource) {
        return $resource('/api/poll/:id/:action', {'id' : '@id'}, {
            'full' : {'method':'GET', 'params' : {'action':'full'}},
            'view' : {'method':'GET', 'params' : {'action':'view'}},
            'edit' : {'method':'POST', 'params' : {'action':'edit'}},
            'vote' : {'method':'POST', 'params' : {'action':'vote'}},
            'delete' : {'method':'POST', 'params' : {'action':'delete'}}
        });
    }).factory('Quest', function($resource) {
        return $resource('/api/question/:id/:action', {'id' : '@id'}, {
            'delete' : {'method':'POST', 'params' : {'action':'delete'}}
        });
    }).factory('Answer', function($resource) {
        return $resource('/api/answer/:id/:action', {'id' : '@id'}, {
            'delete' : {'method':'POST', 'params' : {'action':'delete'}}
        });
    })
;