var index = angular.module('index', ['myUtils','myModels'])
    .config(['$routeProvider', function($routeProvider) {

        $routeProvider
            .when('/poll', {'templateUrl' : '/public/html/list.html', controller: Poll.ListCtrl})
            .when('/poll/:id', {'templateUrl' : '/public/html/vote.html', controller: Poll.VoteCtrl})
            .when('/poll/view/:id', {'templateUrl' : '/public/html/view.html', controller: Poll.ViewCtrl})
            .when('/admin/', {'templateUrl' : '/public/html/admin-list.html', controller: Poll.AdminListCtrl})
            .when('/admin/edit/:id', {'templateUrl' : '/public/html/admin-edit.html', controller: Poll.AdminEditCtrl})
            .when('/admin/view/:id', {'templateUrl' : '/public/html/admin-view.html', controller: Poll.AdminViewCtrl})

            .otherwise({redirectTo:'/poll'})
        ;

//        $routeProvider.otherwise({redirectTo:'/poll'})
    }])
    ;

function Poll($scope) {

}


Poll.ListCtrl = function ($scope, Poll) {

    $scope.ready = false;

    $scope.polls = [];

    // Load polls
    Poll.query(function(items) {
        $scope.ready = true;

        $.map(items, function(item) {
            if (item.status == 1) {
                $scope.polls.push(item);
            }
        });
    });

};



Poll.VoteCtrl = function($scope, $location, $routeParams, Poll) {

    $scope.ready = false;

    new Poll({'id': $routeParams.id}).$full(function (data) {
        $scope.poll = data;
        $scope.ready = true;
    });

    $scope.select = function(answer) {
        var i;
        $.map($scope.poll.quests, function(quest) {
            if (quest.id === answer.question_id) {
                quest.checked = quest.multiply == false ? [] : quest.checked || [];
                // Search answer
                i = quest.checked.indexOf(answer);

                if (i > -1) {
                    quest.checked.splice(i, 1);
                } else {
                    quest.checked.push(answer);
                }

                console.log(quest.checked);

            }
        });
    };

    $scope.submit = function(form) {
        $.map($scope.poll.quests, function(quest) {
            if (quest.required == true) {
                form.$valid = false;
                if (quest.checked) {
                    form.$valid = quest.checked.length > 0;
                }
                console.log(form.$valid, quest.checked, quest.checked.length == 1);
            }
        });
        if (form.$valid) {
            $scope.poll.$vote();
            $location.path('/poll/view/' + $routeParams.id).replace()
        }
        form.$valid = true;
    };

};


Poll.ViewCtrl = function($scope, $routeParams, Poll) {

    new Poll({'id': $routeParams.id}).$view(function (data) {
        $scope.poll = data;
        $scope.ready = true;
    });

    $scope.ready = false;
};


Poll.AdminListCtrl = function($scope, Poll) {

    $scope.type = 3;
    $scope.ready = false;

    $scope.polls = [];
    $scope.pollsAll = [];


    $scope.delete = function($poll) {
        $scope.polls.splice($scope.polls.indexOf($poll), 1);
        $poll.$delete();
    };


    $scope.toggle = function($poll) {
        $poll.status = $poll.status == 0 ? 1 : 0;
        $poll.$save();
        $scope.filter($scope.type);
    };


    $scope.filter = function(type) {
        $scope.type = type;
        if (type == 3) {
            $scope.polls = $scope.pollsAll;
        } else {
            $scope.polls = $.map($scope.pollsAll, function(poll) {
                if (poll.status == type) {
                    return poll;
                }
            });
        }
    };

    // Load polls
    Poll.query(function(items) {
        $scope.ready = true;
        $scope.polls = items;
        $scope.pollsAll = items;
    });

};



Poll.AdminViewCtrl = function($scope, $routeParams, Poll) {

    new Poll({'id': $routeParams.id}).$view(function (data) {
        $scope.poll = data;
        $scope.ready = true;
    });

    $scope.ready = false;
};


Poll.AdminEditCtrl = function ($scope, $location, $routeParams, Poll, Quest, Answer) {

    // Current poll id
    $scope.id = null;

    // Default poll
    $scope.poll = null;


    $scope.ready = false;




    $scope.addQuest = function($poll, data) {
        data = data || {};
        data.required = !!data.required;
        data.multiply = !!data.multiply;
        data = new Quest(data);

        $scope.addAnswer(data);
        $scope.addAnswer(data);

        $poll.quests.push(data);
    };


    $scope.addAnswer = function($quest, data) {
        if ($quest.answers == null)
            $quest.answers = [];

        $quest.answers.push(new Answer());
    };


    $scope.deleteQuest = function($quest) {
        if ($quest.id) {
            $quest.$delete();
        }
        $scope.poll.quests.splice($scope.poll.quests.indexOf($quest), 1);

        if ($scope.poll.quests.length < 2) {
            $scope.addQuest($scope.poll);
        }
    };


    $scope.deleteAnswer = function($answer) {
        if ($answer.id) {
            $answer.$delete();
        }

        $.map($scope.poll.quests, function(quest) {
            var i = quest.answers.indexOf($answer);
            if (i > -1) {
                quest.answers.splice(i, 1);
                if (quest.answers.length < 2 || quest.answers.length == i) {
                    $scope.addAnswer(quest);
                }
            }
        });
    };

    $scope.init = function($poll) {
        if ($poll.quests == null)
            $poll.quests = [];

        if ($poll.quests.length > 0) {
            $.map($poll.quests, function(quest, k) {
                $poll.quests[k] = new Quest(quest);
            });
            if ($poll.quests.length < 2) {
                $scope.addQuest($poll);
            }
        } else {
            $scope.addQuest($poll);
            $scope.addQuest($poll);
        }

        $.map($poll.quests, function(quest, k) {
            if (quest.answers == null)
                quest.answers = [];

            if (quest.answers.length > 0) {
                $.map(quest.answers, function(answer, i) {
                    quest.answers[i] = new Answer(answer);
                });
            } else {
                $scope.addAnswer(quest);
                $scope.addAnswer(quest);
            }
            $poll.quests[k] = quest;
        });

        $scope.poll = $poll;
    };

    if ($routeParams.id) {
        $scope.poll = Poll.full({'id': $routeParams.id}, function(data) {
            $scope.init(data);
            $scope.ready = true;
        });
    } else {
        $scope.init(new Poll({
            'status' : 0,
            'quests' : []
        }));
        $scope.ready = true;
    }

    $scope.submit = function(form) {

        if (form.$valid) {
            if ($scope.poll.quests.length < 2) {
                form.$valid = false;
            }
            $.map($scope.poll.quests, function (quest) {
                $.map(quest.answers, function (answer, i) {
                    if (!answer.text && i < 2) {
                        console.log(answer, i);
                        form.$valid = false;
                    }
                });

                if (quest.answers.length < 2) {
                    form.$valid = false;
                }
            });
            $scope.poll.$edit();
            $location.path('/admin/');
            form.$valid = true;
        }
    };
};