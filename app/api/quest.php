<?php


$app->get('/api/question/{id}', function($id = null) use ($app) {
    $data = $app->quest->findByPoll($id);
    $app->response->setJsonContent($data);
});


$app->post('/api/question/{id}/delete', function($id) use ($app) {
    if ($app->quest->delete($id)) {

    } else {
        throw new \Smart\Web\Error('Ошибка при удалении вопроса');
    }
});