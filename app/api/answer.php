<?php


$app->get('/api/answer/{id}', function($id = null) use ($app) {
    $data = $app->quest->findByPoll($id);
    $app->response->setJsonContent($data);
});


$app->post('/api/answer/{id}/delete', function($id) use ($app) {

    if ($app->quest_answer->delete($id)) {

    } else {
        throw new \Smart\Web\Error(500, 'Ошибка при удалении варианта ответа');
    }

});