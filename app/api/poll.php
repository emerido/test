<?php

/**
 * Returns list of polls or one poll
 */
$app->get('/api/poll(/{id})', function($id = null) use ($app) {

    if ($id) {
        $data = $app->poll->findOne($id);
    } else {
        $data = $app->poll->findAll();
    }

    if ($data) {
        $app->response->setJsonContent($data);
    } else {
        throw new \Smart\Web\Error(404, "Нет данных для отображения");
    }
});

$app->get('/api/poll/{id}/full/', function($id) use ($app) {
    if ($id) {

        $poll = $app->poll->findFull($id);

        $app->response->setJsonContent($poll);
    } else {
        throw new \Smart\Web\Error(404, "Ошибка при удалении опроса");
    }
});

/**
 * Create or update polls
 */
$app->post('/api/poll(/{id})', function($id = null) use ($app) {

    $data = $app->request->getJsonContent();

    if (isset($data['name']) and isset($data['status'])) {

        if ($id) {
            $result = $app->poll->update($id, $data);
        } else {
            $result = $app->poll->create($data);
        }

        if ($result) {
            $app->response->setJsonContent($result);
        } else {
            throw new \Smart\Web\Error(404, "Ошибка записи опроса");
        }
    }

});

/**
 * Edit polls
 */
$app->post('/api/poll(/{id})/edit', function($id = null) use ($app) {

    $data = $app->request->getJsonContent();

    if ($id) {
        $poll = $app->poll->update($id, $data);
    } else {
        $poll = $app->poll->create($data);
    }
    $poll['quests'] = [];

    foreach ($data['quests'] as $k => $quest) {
        $quest['poll_id'] = $poll['id'];
        if (isset($quest['id'])) {
            $item = $app->quest->update($quest['id'], $quest);
        } else {
            $item = $app->quest->create($quest);
        }

        $item['answers'] = [];
        foreach ($quest['answers'] as $i => $answer) {
            $answer['question_id'] = $item['id'];
            if (isset($answer['id'])) {
                $answer = $app->quest_answer->update($answer['id'], $answer);
            } else {
                $answer = $app->quest_answer->create($answer);
            }



            $item['answers'][$i] = $answer ? : ['text' => ''];
        }

        $poll['quests'][$k] = $item;
    }


    $app->response->setJsonContent($poll);

});

/**
 * Edit polls
 */
$app->post('/api/poll/{id}/vote', function($id = null) use ($app) {

    $data = $app->request->getJsonContent();


    foreach ($data['quests'] as $quest) {

        foreach ($quest['checked'] as &$vote) {
            $vote['poll_id'] = $data['id'];
            $vote['answer_id'] = $vote['id'];
            $vote = $app->poll_answer->create($vote);
        }
    }

    $app->response->setJsonContent($data);

});


$app->post('/api/poll/{id}/delete/', function($id) use ($app) {
    if ($app->poll->delete($id)) {
        $app->response->setJsonContent([
            'id'    => $id,
            'error' => false,
            'message' => 'Опрос удален',
        ]);
    } else {
        throw new \Smart\Web\Error(404, "Ошибка при удалении опроса");
    }
});


/**
 * Edit polls
 */
$app->get('/api/poll/{id}/view', function($id) use ($app) {

    $data = $app->poll->findFull($id);

    $app->response->setJsonContent($data);

});