<?php require_once __DIR__ . '/../library/Smart/Loader.php';

$loader = new \Smart\Loader();

$loader->addDirectory([
    __DIR__ . '/../library/',
    __DIR__ . '/../app/model/',
]);