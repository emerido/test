<div ng-controller="Poll.ListCtrl">


    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th style="width: 400px;">Название</th>
            <th>Статус</th>
            <th>Операции</th>
        </tr>
        </thead>
        <tbody ng-show="ready" style="display: none;">
        <tr ng-repeat="poll in polls">
            <td>{{ poll.id }}</td>
            <td>{{ poll.name }}</td>
            <td my-status="poll.status"></td>
            <td>
                <div class="btn-group pull-right">
                    <a href="/admin/edit/{{ poll.id }}" class="btn btn-mini"><i class="icon icon-pencil"></i></a>
                    <a href="/admin/view/{{ poll.id }}" class="btn btn-mini" ng-show="poll.status != 0"><i class="icon icon-eye-open"></i></a>
                    <button ng-click="delete(poll)" class="btn btn-mini"><i class="icon icon-trash"></i></button>
                    <button ng-click="toggle(poll)" class="btn btn-mini">
                        <i class="icon" ng-class="{false:'icon-ban-circle',true:'icon-ok-circle'}[poll.status == 0]"></i>
                    </button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <span ng-hide="ready" style="display: none;">Загрузка...</span>
</div>