<form class="form-horizontal" method="/">

    <? foreach ($poll['quests'] as $quest) { ?>
    <fieldset>

        <legend><?echo $quest['text']?><? if ($quest['required']) echo ' *';?></legend>

        <div class="control-row">

            <? foreach ($quest['answers'] as $answer) { ?>

                <? if ($quest['multiply'])  { ?>
                    <label class="checkbox">
                        <input type="checkbox" name="poll[<?echo $quest['id']?>][]" value="<?echo $answer['id']?>"/> <?echo $answer['text']?>
                    </label>
                <? } else { ?>
                    <label class="radio">
                        <input type="radio" name="poll[<?echo $quest['id']?>]" value="<?echo $answer['id']?>"/> <?echo $answer['text']?>
                    </label>
                <? } ?>
            <? } ?>

        </div>

    </fieldset>
    <? } ?>
</form>