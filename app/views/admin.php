<!DOCTYPE html>
<html ng-app="admin">
<head>
    <title><?= $title; ?></title>

    <link rel="stylesheet" href="/public/css/default.css"/>
    <link rel="stylesheet" href="/public/css/bootstrap.min.css"/>
</head>
<body>
    <div class="container">

        <div class="row">

            <div class="span12">
                <div class="navbar">
                    <div class="navbar-inner" style="width: auto;margin: 20px 0 10px">
                        <a class="brand" href="/">Test</a>
                        <ul class="nav">
                            <li><a href="/">Вопросы</a></li>
                            <li class="active"><a href="/admin/">Администратор</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="span3">
                <div class="well" style="max-width: 340px; padding: 8px 0;">
                    <ul class="nav nav-list">
                        <li class="nav-header">Опросы</li>
                        <li><a href="/admin/">Список</a></li>
                        <li><a href="/admin/edit/">Создать</a></li>
                    </ul>
                </div>
            </div>

            <div class="span9">
                <?= $content; ?>
            </div>

        </div>

    </div>

    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/js/angular.min.js"></script>
    <script type="text/javascript" src="/public/js/angular-resource.min.js"></script>
    <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="/public/ng/module.js"></script>
    <script type="text/javascript" src="/public/ng/admin.js"></script>
</body>
</html>