<!DOCTYPE html>
<html ng-app="index">
<head>
    <title><?= $title; ?></title>

    <link rel="stylesheet" href="/public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/public/css/default.css"/>
</head>
<body>
<div class="container">

    <div class="row">

        <div class="span12">
            <div class="navbar">
                <div class="navbar-inner" style="width: auto;margin: 20px 0 10px">
                    <a class="brand" href="/">Test</a>
                    <ul class="nav">
                        <li class="active"><a href="#/">Вопросы</a></li>
                        <li><a href="#/admin/">Администратор</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="row">

        <div ng-view></div>

    </div>

    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/js/angular.min.js"></script>
    <script type="text/javascript" src="/public/js/angular-resource.min.js"></script>
    <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/public/ng/module.js"></script>
    <script type="text/javascript" src="/public/ng/index.js"></script>
</body>
</html>