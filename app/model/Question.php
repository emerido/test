<?php


class Question
{

    /**
     * DB
     *
     * @var \PDO
     */
    protected $db;


    public function __construct(\Smart\Web\Service $service)
    {
        $this->db = $service->db;
    }


    public function findOne($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `question` WHERE `question`.`id`=:id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetch();
    }


    public function findAll($status)
    {
        $sql = $this->db->prepare('SELECT * FROM `question` ORDER BY id');
        $sql->execute();
        return $sql->fetchAll();
    }


    public function findByPoll($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `question` WHERE `question`.`poll_id`=:id ORDER BY id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetchAll();
    }


    public function create($data)
    {
        $sql = $this->db->prepare('INSERT INTO `question` (id,text,multiply,required,poll_id) VALUES (NULL,:text,:multiply,:required,:poll_id)');
        $sql->bindParam(':text', $data['text']);
        $sql->bindParam(':required', $data['required'], PDO::PARAM_BOOL);
        $sql->bindParam(':multiply', $data['multiply'], PDO::PARAM_BOOL);
        $sql->bindParam(':poll_id', $data['poll_id'], PDO::PARAM_INT);

        $result = $sql->execute();

        if ($result) {
            return $this->findOne($this->db->lastInsertId());
        }
        return false;
    }


    public function update($id, $data)
    {
        $sql = $this->db->prepare('UPDATE `question` SET `text`=:text, `multiply`=:multiply, `required`=:required, `poll_id`=:poll_id WHERE `id`=:id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->bindParam(':text', $data['text']);
        $sql->bindParam(':poll_id', $data['poll_id'], PDO::PARAM_INT);
        $sql->bindParam(':required', $data['required']);
        $sql->bindParam(':multiply', $data['multiply']);

        $result = $sql->execute();

        if ($result) {
            return $data;
        }
        return false;
    }


    public function delete($id)
    {
        $sql = $this->db->prepare('DELETE FROM `question` WHERE `question`.`id`=:poll');
        $sql->bindParam(':poll', $id, PDO::PARAM_INT);
        return $sql->execute();
    }

}