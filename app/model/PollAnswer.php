<?php


class PollAnswer
{

    /**
     * DB
     *
     * @var \PDO
     */
    protected $db;


    public function __construct(\Smart\Web\Service $service)
    {
        $this->db = $service->db;
    }


    public function findOne($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `poll_answers` a WHERE a.`id`=:id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetch();
    }


    public function findByPoll($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `poll_answers` a WHERE a.`poll_id`=:id ORDER BY id DESC');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function findByQuest($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `poll_answers` a WHERE a.`question_id`=:id ORDER BY id DESC');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetchAll();
    }


    public function create($data)
    {
        $sql = $this->db->prepare('INSERT INTO `poll_answers` (id,poll_id,answer_id,question_id) VALUES (NULL,:poll,:answer,:question)');
        $sql->bindParam(':poll', $data['poll_id']);
        $sql->bindParam(':answer', $data['answer_id']);
        $sql->bindParam(':question', $data['question_id']);

        if ($sql->execute()) {
            return $this->findOne($this->db->lastInsertId());
        }
        return false;
    }


    public function update($id, $data)
    {
        $sql = $this->db->prepare('UPDATE `poll_answers` a SET a.`poll_id`=:poll, `poll`.`status`=:status WHERE `poll`.`id`=:id');
        $sql->bindParam(':id', $id);
        $sql->bindParam(':name', $data['name']);
        $sql->bindParam(':status', $data['status']);

        if ($sql->execute()) {
            return $data;
        }
        return false;
    }


    public function delete($id)
    {
        $sql = $this->db->prepare('DELETE FROM `poll_answers` a WHERE a.`id`=:id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        return $sql->execute();
    }

}