<?php


class QuestionAnswer
{

    /**
     * DB
     *
     * @var \PDO
     */
    protected $db;


    public function __construct(\Smart\Web\Service $service)
    {
        $this->db = $service->db;
    }


    public function findOne($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `question_answers` a WHERE a.`id`=:id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetch();
    }


    public function findAll($status)
    {
        $sql = $this->db->prepare('SELECT * FROM `question_answers` ORDER BY id DESC');
        $sql->execute();
        return $sql->fetchAll();
    }


    public function findByQuest($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `question_answers` a WHERE a.`question_id`=:id ORDER BY id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetchAll();
    }


    public function create($data)
    {
        if (empty($data['text'])) {
            return false;
        }

        $sql = $this->db->prepare('INSERT INTO `question_answers` (id,text,question_id) VALUES (NULL,:text,:question_id)');
        $sql->bindParam(':text', $data['text']);
        $sql->bindParam(':question_id', $data['question_id']);

        $result = $sql->execute();

        if ($result) {
            return $this->findOne($this->db->lastInsertId());
        }
        return false;
    }


    public function update($id, $data)
    {
        $sql = $this->db->prepare('UPDATE `question_answers` a SET `text`=:text WHERE `id`=:id');
        $sql->bindParam(':id', $id);
        $sql->bindParam(':text', $data['text']);

        $result = $sql->execute();

        if ($result) {
            return $data;
        }
        return false;
    }


    public function delete($id)
    {
        $sql = $this->db->prepare('DELETE FROM `question_answers` WHERE `question_answers`.`id`=:id');
        $sql->bindParam(':id', $id, PDO::PARAM_INT);
        return $sql->execute();
    }
}