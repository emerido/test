<?php


class Poll
{

    /**
     * DB
     *
     * @var \PDO
     */
    protected $db;


    protected $service;


    public function __construct(\Smart\Web\Service $service)
    {
        $this->db = $service->db;
        $this->service = $service;
    }


    public function findOne($id)
    {
        $sql = $this->db->prepare('SELECT * FROM `poll` WHERE `poll`.`id`=:poll');
        $sql->bindParam(':poll', $id, PDO::PARAM_INT);
        $sql->execute();
        return $sql->fetch();
    }


    public function findAll()
    {
        $sql = $this->db->prepare('SELECT * FROM `poll` ORDER BY id DESC');
        $sql->execute();
        return $sql->fetchAll();
    }


    public function findFull($id)
    {
        $poll = $this->findOne($id);

        if ($poll) {

            $poll['quests'] = $this->service->quest->findByPoll($id);

            foreach ($poll['quests'] as &$quest) {
                $quest['results'] = $this->service->poll_answer->findByQuest($quest['id']);
                $quest['answers'] = $this->service->quest_answer->findByQuest($quest['id']);
            }

        }

        return $poll;
    }


    public function create($data)
    {
        $sql = $this->db->prepare('INSERT INTO `poll` (id,name,status) VALUES (NULL,:name,:status)');
        $sql->bindParam(':name', $data['name']);
        $sql->bindParam(':status', $data['status']);

        if ($sql->execute()) {
            return $this->findOne($this->db->lastInsertId());
        }
        return false;
    }


    public function update($id, $data)
    {
        $sql = $this->db->prepare('UPDATE `poll` SET `poll`.`name`=:name, `poll`.`status`=:status WHERE `poll`.`id`=:id');
        $sql->bindParam(':id', $id);
        $sql->bindParam(':name', $data['name']);
        $sql->bindParam(':status', $data['status']);

        if ($sql->execute()) {
            return $data;
        }
        return false;
    }


    public function delete($id)
    {
        $sql = $this->db->prepare('DELETE FROM `poll` WHERE `poll`.`id`=:poll');
        $sql->bindParam(':poll', $id, PDO::PARAM_INT);
        return $sql->execute();
    }

}