<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following rules:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart;


/**
 * Loader
 * @package Smart
 * @author  Shvorak Alexey
 * @since   0.0.1
 */
class Loader
{


    /**
     * Handling namespace
     * @var null|string
     */
    protected $_namespace = '';


    /**
     * Transform class file path to lowercase
     * @var bool
     */
    protected $_lowercase = false;


    /**
     * File extension
     * @var string
     */
    protected $_extension = '.php';


    /**
     * Source directories
     * @var array
     */
    protected $_directory = array();


    /**
     * Class namespace separators
     * @var array
     */
    protected $_separator = array('_', '\\');


    /**
     * Create loader
     * @param string $name [optional]
     * @param string $path [optional]
     */
    public function __construct($name = null, $path = null)
    {
        if ($name) {
            // Set namespace
            $this->setNamespace((string) $name);
        }

        if ($path) {
            // Set source directory
            $this->setDirectory((string) $path);
        }

        // Register autoload method
        $this->register();
    }


    /**
     * Importing needed class file
     *
     * @param string $class - Needed class name
     *
     * @return bool
     */
    private function import($class)
    {
        if ('' === $this->_namespace or strstr($class, $this->_namespace)) {
            // Search class file
            if ($file = $this->search($class)) {
                // Import class file
                require($file);
            }
        }
        return false;
    }


    /**
     * Search class file
     *
     * @param string $class - Needed class name
     *
     * @return bool|string
     */
    private function search($class)
    {
        if (is_string($class)) {

            // Create file path
            $file = str_replace($this->_separator, DIRECTORY_SEPARATOR, ltrim($class, '\\')) . $this->_extension;

            // Modify file string case
            if ($this->_lowercase) {
                $file = strtolower($file);
            }
            $file = ltrim($file, DIRECTORY_SEPARATOR);
            // Get directories and reverse directories
            $dirs = array_reverse($this->_directory);

            foreach ($dirs as $path) {
                $path = $path . DIRECTORY_SEPARATOR . $file;
                if (file_exists($path)) {
                    return $path;
                }
            }
        }
        return false;
    }


    /**
     * Register autoloader method
     *
     * @return \Smart\Loader
     */
    public function register()
    {
        spl_autoload_register(array($this, 'import'));
        return $this;
    }


    /**
     * Unregister autoloader method
     *
     * @return \Smart\Loader
     */
    public function unregister()
    {
        spl_autoload_unregister(array($this, 'import'));
        return $this;
    }


    /**
     * Set class files extension
     *
     * @param string $name
     *
     * @return \Smart\Loader
     */
    public function setExtension($name)
    {
        if (is_string($name)) {
            $this->_extension = $name;
        }
        return $this;
    }


    /**
     * Returns class files extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->_extension;
    }


    /**
     * Sets source directory for files search
     *
     * @param string|array $path
     *
     * @return \Smart\Loader
     */
    public function setDirectory($path)
    {
        $this->_directory = array();
        return $this->addDirectory($path);
    }


    /**
     * Add additional source directory for files search
     *
     * @param string|array $path
     *
     * @return \Smart\Loader
     */
    public function addDirectory($path)
    {
        if (is_string($path)) {
            $this->_directory[] = preg_replace('#/|\\\#m', DIRECTORY_SEPARATOR, rtrim($path, DIRECTORY_SEPARATOR));
        } elseif (is_array($path)) {
            foreach ($path as $dir) {
                $this->addDirectory($dir);
            }
        }
        return $this;
    }


    /**
     * Returns array of source directories
     *
     * @return array
     */
    public function getDirectory()
    {
        return $this->_directory;
    }


    /**
     * Sets classes namespace separator (\ or _)
     *
     * @param string $char
     *
     * @return \Smart\Loader
     */
    public function setSeparator($char)
    {
        if (is_string($char)) {
            $this->_separator = array($char);
        }
        return $this;
    }


    /**
     * Add additional class separator
     *
     * @param string $char
     *
     * @return \Smart\Loader
     */
    public function addSeparator($char)
    {
        if (is_string($char)) {
            $this->_separator[] = $char;
        }
        return $this;
    }


    /**
     * Returns classes namespace separator
     *
     * @return array
     */
    public function getSeparator()
    {
        return $this->_separator;
    }


    /**
     * Set classes namespace
     *
     * @param string $name
     *
     * @return \Smart\Loader
     */
    public function setNamespace($name)
    {
        if (is_string($name)) {
            $this->_namespace = $name;
        }
        return $this;
    }


    /**
     * Returns classes namespace
     *
     * @return null|string
     */
    public function getNamespace()
    {
        return $this->_namespace;
    }


    /**
     * Sets class path case modification
     *
     * @param boolean $bool
     *
     * @return \Smart\Loader
     */
    public function setLowercase($bool)
    {
        $this->_lowercase = (bool) $bool;
        return $this;
    }
}