<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart;

use Closure;
use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;


/**
 * Class Service
 *
 * Service container
 *
 * @property \Smart\Config config
 *
 * @package Smart
 */
class Service implements ArrayAccess, IteratorAggregate
{

    /**
     * Services container
     *
     * @var array
     */
    protected $values= [];


    /**
     * Creates one result closure
     *
     * @param callable $func
     *
     * @return callable
     */
    public function share(Closure $func)
    {
        return function ($service) use ($func) {
            static $cache;

            if (!$cache) {
                $cache = $func($service);
            }
            return $cache;
        };
    }


    /**
     * Creates service closure
     *
     * @param callable $func
     *
     * @return callable
     */
    public function protect(Closure $func)
    {
        return function ($service) use ($func) {
            return $func($service);
        };
    }


    /**
     * Returns all service values
     *
     * @return array
     */
    public function values()
    {
        return $this->values;
    }


    /**
     * Default configuration
     *
     * @return void
     */
    public function defaults()
    {
        $this['config'] = $this->share(function () {
            return new Config();
        });

        $this['emitter'] = $this->share(function () {
            return new Emitter();
        });
    }


    /**
     * Magic set facade
     *
     * @param string $id
     * @param mixed $value
     *
     * @return void
     */
    public function __set($id, $value)
    {
        $this->set($id, $value);
    }


    /**
     * Magic get facade
     *
     * @param string $id
     *
     * @return mixed|null
     */
    public function __get($id)
    {
        return $this->get($id);
    }


    /**
     * @param string $id        Service name
     * @param $args             Service method arguments
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function __call($id, $args)
    {
        $service = $this->get($id);

        if ($service and method_exists($service, '__invoke')) {
            return call_user_func_array([$service, '__invoke'], $args);
        }
        throw new \Exception('Service ' . $id . ' not found or not callable');
    }


    /**
     * Service instantiate
     *
     * @param bool $default     Initialize default services ? (default=true)
     */
    public function __construct($default = true)
    {
        if ($default) {
            $this->defaults();
        }
    }

    
    /**
     * Creates service
     *
     * @param string $id        Service name
     * @param mixed $value      Service value
     * @param bool $shared      This is sha
     *
     * @throws \Exception
     *
     * @return \Smart\Service
     */
    public function set($id, $value, $shared = false)
    {
        if (!is_string($id)) {
            throw new \Exception('Service name must be a string, ' . gettype($id) . ' given');
        }

        if ($value instanceof Closure) {
            $value = $shared
                ? $this->share($value)
                : $this->protect($value)
            ;
        }

        $this->values[$id] = $value;

        return $this;
    }


    /**
     * Returns service
     *
     * @param string $id
     *
     * @return null|mixed
     */
    public function get($id)
    {
        if (isset($this->values[$id])) {
            $value = $this->values[$id];

            if ($value instanceof Closure) {
                $value = $value($this);
            }
            return $value;
        }
        return null;
    }


    /**
     * Returns service existing
     *
     * @param string $id
     *
     * @return bool
     */
    public function has($id)
    {
        return isset($this->values[$id]);
    }


    /**
     * Delete service
     *
     * @param string $id
     *
     * @return void
     */
    public function del($id)
    {
        if (isset($this->values[$id])) {
            unset($this->values[$id]);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function offsetSet($id, $value)
    {
        $this->set($id, $value);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetGet($id)
    {
        return $this->get($id);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetUnset($id)
    {
        $this->del($id);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetExists($id)
    {
        return $this->has($id);
    }


    /**
     * {@inheritdoc}
     */
    public function getIterator() {
        return new ArrayIterator($this->values);
    }

}