<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following rules:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart;


use ArrayAccess;



class Config implements ArrayAccess
{

    /**
     * Config values
     *
     * @var array
     */
    protected $values = array();


    /**
     * Instantiate config
     *
     * @param array $values
     */
    public function __construct(array $values = array())
    {
        $this->set($values);
    }


    /**
     * Sets config values
     *
     * @param array $values
     */
    public function __invoke(array $values)
    {
        $this->set($values);
    }


    /**
     * Sets config option
     *
     * @param Config|string|array $key
     * @param null|mixed $val
     *
     * @throws \Exception
     *
     * @return \Smart\Config
     */
    public function set($key, $val = null)
    {
        if (is_string($key)) {
            if (is_array($val)) {
                $val = new Config($val);
            }
            $this->values[$key] = $val;
        } elseif (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->set($k, $v);
            }
        } elseif ($key instanceof Config) {

        } else {
            throw new \Exception('Key argument must be a string, array or \\Smart\\Config instance.' . gettype($key) . ' given.');
        }

        return $this;
    }


    /**
     * Returns config option
     *
     * @param string $key
     * @param null|mixed $def
     *
     * @return null|mixed
     */
    public function get($key, $def = null)
    {
        if (is_string($key) and isset($this->values[$key])) {
            return $this->values[$key];
        }
        return $def;
    }


    /**
     * Returns option existing
     *
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return (is_string($key) and in_array($key, $this->values));
    }


    /**
     * Unset config option
     *
     * @param string $key
     *
     * @return \Smart\Config
     */
    public function del($key)
    {
        if ($this->has($key)) {
            unset($this->values[$key]);
        }
        return $this;
    }


    /**
     * Returns all config options as array
     *
     * @return array
     */
    public function values()
    {
        $out = array();
        foreach ($this->values as $key => $val) {
            if ($val instanceof self) {
                $val = $val->values();
            }
            $out[$key] = $val;
        }
        return $out;
    }


    /**
     * {@inheritdoc}
     */
    public function offsetSet($id, $value)
    {
        $this->set($id, $value);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetGet($id)
    {
        return $this->get($id);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetExists($id)
    {
        return $this->has($id);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetUnset($id)
    {
        $this->del($id);
    }

}