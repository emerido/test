<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;


class View
{

    /**
     * View directory
     *
     * @var string
     */
    protected $directory;


    /**
     * View files extension
     *
     * @var string
     */
    protected $extension = '.php';


    /**
     * View layout
     *
     * @var string
     */
    protected $layout;


    /**
     * View values
     *
     * @var array
     */
    protected $values = [];



    public function __construct($directory = null)
    {

    }


    public function __set($name, $value)
    {
        $this->set($name, $value);
    }


    public function __get($name)
    {
        return $this->get($name);
    }


    public function set($name, $value = null)
    {
        if (is_string($name)) {
            $this->values[$name] = $value;
        } elseif (is_array($name)) {
            foreach($name as $k => $v) {
                $this->set($k, $v);
            }
        }
    }


    public function get($name)
    {
        if (isset($this->values[$name])) {
            return $this->values[$name];
        }
        return null;
    }


    public function render($file)
    {
        $info = pathinfo($file);
        $file = $info['filename'];

        if (!isset($info['extension'])) {
            $file .= $this->extension;
        }

        if ($info['dirname'] === '.') {
            $file = $this->directory . $file;
        } else {
            $file = $this->directory . $info['dirname'] . DIRECTORY_SEPARATOR . $file;
        }

        if (is_readable($file)) {
            ob_start();
            extract($this->values);
            include $file;
            $content = ob_get_clean();
        } else {
            throw new \Exception('Can\'t read template file ' . $file);
        }

        if ($this->layout) {
            $this->set('content', $content);
            $file = $this->layout;
            $this->layout = null;
            $content = $this->render($file);
        }


        return $content;
    }


    public function layout($file)
    {
        $this->layout = $file;
        return $this;
    }


    public function setExtension($ext)
    {
        if (is_string($ext)) {
            if (substr($ext, 0, 1) !== '.') {
                $ext = '.' . $ext;
            }
            $this->extension = $ext;
        }
    }


    public function setDirectory($path)
    {
        if (is_dir($path)) {
            $this->directory = realpath($path) . DIRECTORY_SEPARATOR;
        }
        return $this;
    }


    public function getDirectory()
    {
        return $this->directory;
    }

}