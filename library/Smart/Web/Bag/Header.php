<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following rules:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web\Bag;


use Smart\Web\Bag;


class Header extends Bag
{


    /**
     * Sets storage value
     *
     * @param string|array $id
     *
     * @param null|mixed $value
     *
     * @return \Smart\Web\Bag
     */
    public function set($id, $value = null)
    {
        if (is_string($id)) {
            $id = $this->normalize($id);
        }
        return parent::set($id, $value);
    }


    /**
     * Normalize header key name
     *
     * @param string $header
     *
     * @return string
     */
    public function normalize($header)
    {
        $chains = preg_split('#[_-]+#u', $header);

        foreach ($chains as $key => $val) {
            $chains[$key] = ucfirst(strtolower($val));
        }

        return implode('-', $chains);
    }


    /**
     * Parse Accept* headers
     *
     * @param string $header - Accept header name
     *
     * @return array
     */
    public function getAccept($header)
    {
        if ($this->has($header)) {
            //
            $inc = 1;
            $part = explode(',', $this->get($header));
            $result = array();

            foreach ($part as $key => $val) {
                $test = explode(';', $val);
                if (isset($test[1])) {
                    $key = (float) str_replace('q=', '', $test[1]);
                } else {
                    $key = $inc++;
                }
                $result[(string) $key] = $test[0];
            }

            return $result;
        }
        return array();
    }

}