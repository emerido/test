<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following rules:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;




/**
 * Class Mime
 * @package Smart\Web
 */
class Mime
{


    public static $formats = array(
        'html' => array('text/html', 'application/xhtml+xml'),
        'txt'  => array('text/plain'),
        'js'   => array('application/javascript', 'application/x-javascript', 'text/javascript'),
        'css'  => array('text/css'),
        'json' => array('application/json', 'application/x-json'),
        'xml'  => array('text/xml', 'application/xml', 'application/x-xml'),
        'rdf'  => array('application/rdf+xml'),
        'atom' => array('application/atom+xml'),
        'rss'  => array('application/rss+xml'),
    );


    /**
     * Return format from mime type
     *
     * @param string $mime
     *
     * @throws \InvalidArgumentException
     *
     * @return string|null
     */
    public static function getFormat($mime)
    {
        if (!is_string($mime)) {
            throw new \InvalidArgumentException('Invalid argument type. $mime must be a string');
        }

        foreach (self::$formats as $format => $mimes) {
            if (in_array($mime, (array) $mimes)) {
                return $format;
            }
        }

        return null;
    }


    /**
     * Return true if format exists
     *
     * @param string $format
     *
     * @return bool
     */
    public static function hasFormat($format)
    {
        return array_key_exists((string) $format, self::$formats);
    }


    /**
     * Return mime type from format
     *
     * @param string $format
     *
     * @throws \InvalidArgumentException
     *
     * @return array|null
     */
    public static function getMime($format)
    {
        if (!is_string($format)) {
            throw new \InvalidArgumentException('Invalid argument type. $format must be a string');
        }

        if (array_key_exists($format, self::$formats)) {
            return self::$formats[$format][0];
        }

        return null;
    }


    /**
     * Returns true if mime exists
     *
     * @param string $mime
     *
     * @return bool
     */
    public static function hasMime($mime)
    {
        foreach (self::$formats as $format => $mimes) {
            if (in_array((string) $mime, (array) $mimes)) {
                return true;
            }
        }
        return false;
    }

}