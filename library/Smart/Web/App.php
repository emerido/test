<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;


use Exception;
use Smart\Config;
use Smart\Service;
use Smart\Web\Error;
use Smart\Web\Service as DefaultService;


/**
 * Class App
 *
 * @property \Smart\Config config
 * @property \Smart\Emitter emitter
 * @property \Smart\Web\Router router
 * @property \Smart\Web\Request request
 * @property \Smart\Web\Response response
 * @property \Smart\Web\View view
 * @property \PDO db
 *
 * @package Smart\Web
 */
class App
{

    /**
     * Service container
     *
     * @var \Smart\Service
     */
    protected $service;


    /**
     * Instantiate application
     *
     * @param Service $service
     */
    public function __construct(Service $service = null)
    {
        if (!$service) {
            $service = new DefaultService();
        }
        $this->setService($service);

        // Set exception handler
        set_exception_handler(function(\Exception $e) {
            $this->emitter->emit('error', [$this, $e]);
        });

        // Set error handler
        set_error_handler(function($code, $message, $file, $line, $context) {
            $this->emitter->emit('errorUser', [$this, $code, $message, $file, $line, $context]);
        });
    }


    /**
     * Method for invokable services
     *
     * @param string $name
     * @param array $args
     *
     * @return mixed
     */
    public function __call($name, $args)
    {
        return $this->service->__call($name, $args);
    }


    /**
     * Adds service
     *
     * @param string $id
     * @param mixed $value
     */
    public function __set($id, $value)
    {
        if (is_string($id)) {
            $this->service[$id] = $value;
        }
    }


    /**
     * Returns service
     *
     * @param string$id
     *
     * @return mixed|null
     */
    public function __get($id)
    {
        return $this->service->get($id);
    }

    
    /**
     * Sets service container
     *
     * @param \Smart\Service $service
     */
    public function setService(Service $service)
    {
        $this->service = $service;
    }


    /**
     * Returns service container
     *
     * @return \Smart\Service
     */
    public function getService()
    {
        return $this->service;
    }


    /**
     * Execute request handling
     *
     * @param string $uri [optional]    Custom url for handling
     *
     * @return \Smart\Web\Response
     */
    public function run($uri = null)
    {
        try {
            if ($uri) {
                $this->request->uri->setPath($uri);
            }

            $route = $this->router->handle(
                $this->request->getPath(),
                $this->request->getMethod()
            );

            if ($route) {
                ob_start();
                // Call before callbacks
                foreach ($route->getBefore() as $before) {
                    call_user_func_array($before, [$this]);
                }
                ob_clean();

                // Call route callback
                $content = null;
                $callback = $route->getCallback();
                if ($callback) {
                    $content = call_user_func_array($callback, $route->getParam());
                }

                if (!is_string($content)) {
                    $content = ob_get_clean();
                }

                // Set content from buffer if response content is empty
                if ($this->response->getContent() == null) {
                    $this->response->setContent($content);
                }

                // Call after callbacks
                foreach ($route->getAfter() as $after) {
                    call_user_func_array($after, [$this]);
                }

                ob_end_clean();

            } else {
                throw new Error(404, 'No matched route');
            }

        } catch (\Exception $e) {
            $code = '';
            if ($e instanceof Error) {
                $code = $e->getCode();
            }
            $this->emitter->emit('error' . $code, [$this, $e]);
        }

        $this->response->sendCookies();
        $this->response->sendHeaders();

        return $this->response;
    }


    /**
     * Adds error callback
     *
     * @param int|callable $code
     * @param null|callable $callback
     *
     * @throws Exception
     */
    public function error($code, $callback = null)
    {
        if ($callback === null) {
            $callback = $code;
            $code = '';
        }

        if (is_callable($callback)) {
            $this->emitter->on('error' . $code, $callback);
        } else {
            throw new Exception('Error callback must be a callable');
        }
    }


    /**
     * Creates route for all request methods
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function map($path, $callback)
    {
        return $this->router->route($path, '*', $callback);
    }


    /**
     * Creates route for GET verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function get($path, $callback)
    {
        return $this->router->route($path, 'GET', $callback);
    }


    /**
     * Creates route for PUT verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function put($path, $callback)
    {
        return $this->router->route($path, 'PUT', $callback);
    }


    /**
     * Creates route for POST verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function post($path, $callback)
    {
        return $this->router->route($path, 'POST', $callback);
    }


    /**
     * Creates route for HEAD verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function head($path, $callback)
    {
        return $this->router->route($path, 'HEAD', $callback);
    }


    /**
     * Creates route for OPTIONS verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function options($path, $callback)
    {
        return $this->router->route($path, 'OPTIONS', $callback);
    }


    /**
     * Creates route for DELETE verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function delete($path, $callback)
    {
        return $this->router->route($path, 'DELETE', $callback);
    }

}