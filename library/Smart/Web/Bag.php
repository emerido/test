<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following rules:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;


use ArrayAccess;
use ArrayIterator;


/**
 * Class Bag
 * @package Smart\Web
 */
class Bag implements ArrayAccess
{

    /**
     * Header records
     *
     * @var array
     */
    protected $values = array();


    /**
     * Instantiate storage
     *
     * @param array $values
     */
    public function __construct(array $values = null)
    {
        if (null !== $values) {
            $this->set($values);
        }
    }


    /**
     * Returns true if record exists
     *
     * @param string $id
     *
     * @return boolean
     */
    public function has($id)
    {
        return array_key_exists((string) $id, $this->values);
    }


    /**
     * Returns storage value
     *
     * @param string $id [optional]
     *
     * @param null|mixed $default
     *
     * @return mixed
     */
    public function get($id, $default = null)
    {
        if ($this->has($id)) {
            return $this->values[$id];
        }
        return $default;
    }


    /**
     * Sets storage value
     *
     * @param string|array $id
     *
     * @param null|mixed $value
     *
     * @return \Smart\Web\Bag
     */
    public function set($id, $value = null)
    {
        if (is_array($id)) {
            foreach ($id as $key => $val) {
                $this->set($key, $val);
            }
        } elseif (is_string($id)) {
            $this->values[$id] = $value;
        }

        return $this;
    }


    /**
     * Unset record
     *
     * @param string $id
     *
     * @return \Smart\Web\Bag
     */
    public function del($id)
    {
        if ($this->has($id)) {
            unset ($this->values[$id]);
        }
        return $this;
    }


    /**
     * Returns all records
     *
     * @return array
     */
    public function all()
    {
        return $this->values;
    }


    /**
     * Cleaning storage values
     *
     * @return void
     */
    public function clear()
    {
        $this->values = array();
    }


    /**
     * Returns values iterator
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->values);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }


    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->del($offset);
    }

}