<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following rules:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;


use Smart\Web\Bag;


class Request
{




    /**
     * Request url data
     *
     * @var \Smart\Web\Uri
     */
    public $uri;


    /**
     * Server variables
     *
     * @var \Smart\Web\Bag\Server
     */
    public $server;


    /**
     * @var \Smart\Web\Bag\Header
     */
    public $header;


    /**
     * @var \Smart\Web\Bag
     */
    public $cookie;

    /**
     * Path query values
     *
     * @var \Smart\Web\Bag
     */
    public $query;


    /**
     * Post variables
     *
     * @var \Smart\Web\Bag
     */
    public $post;


    /**
     * Request format
     *
     * @var string
     */
    public $format;

    /**
     * Request method
     *
     * @var string
     */
    public $method;


    /**
     * Request body data
     *
     * @var string
     */
    public $content;


    /**
     * Instantiate request object
     */
    public function __construct()
    {
        $this->uri      = new Uri($_SERVER);
        $this->post     = new Bag($_POST);
        $this->query    = new Bag($_GET);
        $this->cookie   = new Bag($_COOKIE);
        $this->server   = new Bag\Server($_SERVER);
        $this->header   = new Bag\Header($this->server->getHeaders());
    }



    /**
     * Returns request format
     *
     * @return string
     */
    public function getFormat()
    {
        if (null === $this->format) {
            // You can change default content type
            $type = $this->header->get('Content-Type', 'text/html');

            // Returns format form mime
            $this->format = Mime::getFormat($type);
        }

        return $this->format;
    }


    /**
     * Setup request format
     *
     * @param string $format
     *
     * @return \Smart\Web\Request
     */
    public function setFormat($format)
    {
        if (Mime::hasFormat($format)) {
            $this->format = (string) $format;
        }

        return $this;
    }


    /**
     * Returns request content
     *
     * @return string
     */
    public function getContent()
    {
        if (null === $this->content) {
            $this->content = trim(file_get_contents('php://input'));
        }
        return $this->content;
    }


    public function getJsonContent()
    {
        return json_decode($this->getContent(), true);
    }


    /**
     * Sets request content
     *
     * @param string $content
     *
     * @return \Smart\Web\Request
     */
    public function setContent($content)
    {
        if (is_string($content)) {
            $this->content = $content;
        }
        return $this;
    }


    /**
     * Returns request path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->uri->getPath();
    }


    /**
     * Returns request method
     *
     * @return string
     */
    public function getMethod()
    {
        if (null === $this->method) {
            $this->method = strtoupper($this->server->get('REQUEST_METHOD', 'GET'));
        }
        return $this->method;
    }


    /**
     * Sets request method
     *
     * @param string $method
     *
     * @return \Smart\Web\Request
     */
    public function setMethod($method)
    {
        $this->method = null;
        $this->server->set('REQUEST_METHOD', strtoupper($method));
        return $this;
    }


    /**
     * Returns methods conformity
     *
     * @param string $method
     *
     * @return bool
     */
    public function isMethod($method)
    {
        return $this->getMethod() === (string) $method;
    }


    /**
     * This request is GET
     *
     * @return bool
     */
    public function isGet()
    {
        return $this->isMethod('GET');
    }


    /**
     * This request is PUT
     *
     * @return bool
     */
    public function isPut()
    {
        return $this->isMethod('PUT');
    }


    /**
     * This request is HEAD
     *
     * @return bool
     */
    public function isHead()
    {
        return $this->isMethod('HEAD');
    }


    /**
     * This request is POST
     *
     * @return bool
     */
    public function isPost()
    {
        return $this->isMethod('POST');
    }


    /**
     * This request is DELETE
     *
     * @return bool
     */
    public function isDelete()
    {
        return $this->isMethod('DELETE');
    }


    /**
     * This request is OPTIONS
     *
     * @return bool
     */
    public function isOptions()
    {
        return $this->isMethod('OPTIONS');
    }


    /**
     * Return true if request secure (https)
     *
     * @return bool
     */
    public function isSecure()
    {
        return strtolower($this->server->get('HTTPS')) === 'on';
    }


    /**
     * This request from flash
     *
     * @return bool
     */
    public function isFlash()
    {
        return (strstr(strtolower($this->header->get('User-Agent')), ' flash')) ? true : false;
    }


    /**
     * This request is Async
     *
     * @return bool
     */
    public function isAjax()
    {
        return $this->isXhr();
    }


    /**
     * This request is Async
     *
     * @return bool
     */
    public function isXhr()
    {
        return $this->header->get('X-Requested-With') === 'XMLHttpRequest';
    }

}