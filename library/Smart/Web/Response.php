<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;



class Response
{

    /**
     * Response http code
     *
     * @var int
     */
    protected $code = 200;


    /**
     * Response headers
     *
     * @var \Smart\Web\Bag\Header
     */
    public $header;


    /**
     * Response cookies
     *
     * @var \Smart\Web\Bag\Cookie
     */
    public $cookie;


    /**
     * Response format
     *
     * @var string
     */
    protected $format;


    /**
     * Response charset
     *
     * @var string
     */
    protected $charset = 'utf8';


    /**
     * Response body data
     *
     * @var string
     */
    protected $content;


    public function __construct()
    {
        $this->header = new Bag();
        $this->cookie = new Bag\Cookie();

        $this->format = Mime::getMime('html');
        $this->charset = 'utf-8';
    }


    /**
     * Sets response http code
     *
     * @param int $code
     *
     * @return \Smart\Web\Response
     */
    public function setCode($code)
    {
        if (is_int($code)) {
            $this->code = $code;
        }
        return $this;
    }


    /**
     * Returns response http code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }


    /**
     * Sets response mime type from format
     *
     * @param string $format
     *
     * @return \Smart\Web\Response
     */
    public function setFormat($format)
    {
        $mime = Mime::getMime($format);
        if ($mime) {
            $this->format = $mime;
        }
        return $this;
    }


    /**
     * Returns response mime type
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }


    /**
     * Sets response charset
     *
     * @param string $charset
     *
     * @return \Smart\Web\Response
     */
    public function setCharset($charset)
    {
        if (is_string($charset)) {
            $this->charset = $charset;
        }
        return $this;
    }


    /**
     * Returns response charset
     *
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }


    /**
     * Sets response cookie
     *
     * @param string $name
     * @param mixed|null $value
     * @param int|null $expire
     * @param string|null $path
     * @param string|null $domain
     * @param bool|null $secure
     * @param bool|null $http
     *
     * @return \Smart\Web\Response
     */
    public function setCookie($name, $value = null, $expire = null, $path = null, $domain = null, $secure = null, $http = true)
    {
        $this->cookie->set(
            $name,
            $value,
            $expire,
            $path,
            $domain,
            $secure,
            $http
        );
        return $this;
    }


    /**
     * Sets response header
     *
     * @param string $name
     * @param mixed $value
     *
     * @return \Smart\Web\Response
     */
    public function setHeader($name, $value)
    {
        $this->header->set($name, (string) $value);
        return $this;
    }


    /**
     * Sets response content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = (string) $content;
    }


    public function setJsonContent($content)
    {
        $this->setFormat('json');
        $this->setContent(json_encode($content));
    }


    /**
     * Returns response content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * Send response cookie
     *
     * @return void
     */
    public function sendCookies()
    {
        foreach ($this->cookie->all() as $name => $opt) {
            setcookie($name, $opt['value'], $opt['expire'], $opt['path'], $opt['domain'], $opt['secure'], $opt['http']);
        }
    }


    /**
     * Send response header
     *
     * @return void
     */
    public function sendHeaders()
    {
        \http_response_code($this->getCode());
        $this->header->set('Content-Type', $this->getFormat() . '; charset=' . $this->getCharset());
        foreach ($this->header->all() as $name => $value) {
            header($name . ': ' . $value);
        }
    }


    /**
     * Flush response content
     *
     * @return void
     */
    public function sendContent()
    {
        echo $this->content;
    }


    public function redirect($path, $code = 301)
    {
        $this->setCode($code);
        $this->setHeader('Location', $path);
    }
    
}