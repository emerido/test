<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;


use Smart\Config;
use Smart\Service as BaseService;


/**
 * Class Service
 *
 * Default Web services
 *
 * @package Smart\Web
 *
 * @property \PDO db
 * @property \Smart\Web\Request request
 */
class Service extends BaseService
{

    public function defaults()
    {
        // Setup parent defaults
        parent::defaults();

        // Setup router
        $this['router'] = $this->share(function($service) {
            return new Router($service['config']);
        });

        // Setup request
        $this['request'] = $this->share(function($service) {
            return new Request();
        });

        // Setup response
        $this['response'] = $this->share(function($service) {
            return new Response();
        });

        // Setup view
        $this['view'] = $this->share(function($service) {
            $view = new View();
            $view->setDirectory($service->config['view']['directory']);
            $view->setExtension($service->config['view']['extension']);

            return $view;
        });

        // Setup database
        $this['db'] = $this->share(function($service) {
            $db = new \PDO(
                $service->config['db']['dsn'],
                $service->config['db']['user'],
                $service->config['db']['pass']
            );

            $db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            $db->exec("SET NAMES UTF8");

            return $db;
        });
    }
}