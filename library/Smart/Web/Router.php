<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;
use Smart\Config;
use Smart\Web\Router\Route;

/**
 * Class Router
 * @package Smart\Web
 */
class Router
{

    /**
     * Router config
     *
     * @var \Smart\Config
     */
    protected $config;


    /**
     * Routes list
     *
     * @var \Smart\Web\Router\Route[]
     */
    protected $routes = [];


    /**
     * Matched route
     *
     * @var null|\Smart\Web\Router\Route
     */
    protected $matched;


    /**
     * Instantiate router
     *
     * @param \Smart\Config $config [optional]
     */
    public function __construct($config = null)
    {
        if ($config === null) {
            $config = new Config([
                'strict' => false,
                'sensitive' => false,
            ]);
        }
    }


    /**
     * Search match route
     *
     * @param string $path
     * @param string $verb
     *
     * @return \Smart\Web\Router\Route|null
     */
    public function handle($path, $verb)
    {
        $this->matched = null;
        foreach ($this->routes as $route) {
            if ($route->match($path, $verb)) {
                if ($this->matched === null) {
                    $this->matched = $route;
                } else {
                    if ($this->matched->getOrder() <= $route->getOrder()) {
                        $this->matched = $route;
                    }
                }
            }
        }
        return $this->matched;
    }


    /**
     * Returns matched route
     *
     * @return \Smart\Web\Router\Route
     */
    public function matched()
    {
        return $this->matched;
    }


    /**
     * Creates route for all request methods
     *
     * @param string $path
     * @param null $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function map($path, $callback = null)
    {
        return $this->route($path, '*', $callback);
    }


    /**
     * Creates route for GET verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function get($path, $callback = null)
    {
        return $this->route($path, 'GET', $callback);
    }


    /**
     * Creates route for PUT verbs
     *
     * @param string $path
     * @param callback $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function put($path, $callback = null)
    {
        return $this->route($path, 'PUT', $callback);
    }


    /**
     * Creates route for POST verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function post($path, $callback = null)
    {
        return $this->route($path, 'POST', $callback);
    }


    /**
     * Creates route for HEAD verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function head($path, $callback = null)
    {
        return $this->route($path, 'HEAD', $callback);
    }


    /**
     * Creates route for OPTIONS verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function options($path, $callback)
    {
        return $this->route($path, 'OPTIONS', $callback);
    }


    /**
     * Creates route for DELETE verbs
     *
     * @param string $path
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function delete($path, $callback)
    {
        return $this->route($path, 'DELETE', $callback);
    }


    /**
     * Creates route
     *
     * @param string $path
     * @param string|array $verb
     * @param callable $callback
     *
     * @return \Smart\Web\Router\Route
     */
    public function route($path, $verb, $callback = null)
    {
        $route = new Route($path, $verb);
        if (is_callable($callback)) {
            $route->setCallback($callback);
        }

        return $this->routes[] = $route;
    }

}