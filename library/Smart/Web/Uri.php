<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following rules:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web;


class Uri
{

    const ALNUM = 'a-zа-я0-9';


    /**
     * Original uri
     *
     * @var null|string
     */
    protected $uri = null;


    /**
     * Uri hostname
     *
     * @var string
     */
    protected $host = null;


    /**
     * Uri base hostname
     *
     * @var string
     */
    protected $base_host = null;


    /**
     * Uri port
     *
     * @var null|int
     */
    protected $port = null;


    /**
     * Uri base alias
     *
     * @var null|string
     */
    protected $base = null;


    /**
     * Uri alias
     *
     * @var null|string
     */
    protected $path = null;


    /**
     * Uri parsed query
     *
     * @var array
     */
    protected $query = array();


    /**
     * Uri scheme
     *
     * @var string
     */
    protected $scheme = 'http';


    /**
     * Supported schemes
     *
     * @var array
     */
    protected $schemes = array('http', 'https');


    /**
     * Uri fragment
     *
     * @var null|string
     */
    protected $fragment = null;


    /**
     * Uri valid state
     *
     * @var bool
     */
    protected $valid = false;


    public function __construct($path)
    {
        if (is_array($path)) {
            if ($path === $_SERVER) {
                $this->formServer($path);
            } else {
                $this->formArray($path);
            }
        } elseif (is_string($path)) {
            $this->setPath($path);
        }
    }


    /**
     * Sets uri host
     *
     * @param string $host [optional]
     * @return \Smart\Web\Uri
     */
    public function setHost($host = null)
    {
        if (is_string($host) and $this->validHost($host)) {
            $this->host = $host;
        }
        return $this;
    }


    /**
     * Returns uri host
     *
     * @return null|string
     */
    public function getHost()
    {
        return $this->host;
    }


    /**
     * Returns root domain name
     *
     * @return null|string
     */
    public function getBaseHost()
    {
        return $this->base_host;
    }

    /**
     * Returns uri domains array
     *
     * @return array
     */
    public function getDomains()
    {
        return explode('.', rtrim(str_replace($this->base_host, '', $this->host), '.'));
    }


    /**
     * Sets uri port
     *
     * @param int|string $port [optional]
     * @return \Smart\Web\Uri
     */
    public function setPort($port = null)
    {
        $this->port = (int) $port;
        return $this;
    }


    /**
     * Returns uri port
     *
     * @return int|null
     */
    public function getPort()
    {
        return $this->port;
    }


    /**
     * Sets uri alias
     *
     * @param string $path [optional]
     *
     * @return \Smart\Web\Uri
     */
    public function setPath($path)
    {
        if (is_string($path) and $this->validPath($path)) {
            // Normalize alias
            $this->path = '/' . trim($path, '/');
        }
        return $this;
    }


    /**
     * Returns uri alias
     *
     * @return null|string
     */
    public function getPath()
    {
        return '/' . $this->getRelativePath();
    }


    /**
     * Returns relative alias
     *
     * @param null|string $relative [optional]
     * @return string
     */
    public function getRelativePath($relative = null)
    {
        // Delete base alias
        $path = str_replace($this->base, '', $this->path);

        // Delete optional alias
        if (is_string($relative)) {
            $relative = '/' . trim($relative, '/');
            if (substr($path, - strlen($relative)) === $relative) {
                $path = substr($path, strlen($relative));
            }
        }

        return trim($path, '/');
    }


    /**
     * Returns absolute alias (with base alias)
     *
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return $this->path;
    }


    /**
     * Returns true if alias start with passed alias
     *
     * @param string $path
     *
     * @throws \InvalidArgumentException
     *
     * @return boolean
     */
    public function isPathContains($path)
    {
        if (!is_string($path)) {
            throw new \InvalidArgumentException('Path argument must be a string');
        }
        return (bool) preg_match('#^/?' . $path . '#ui', $this->getPath());
    }


    /**
     * Sets uri base alias
     *
     * @param string $path
     *
     * @return \Smart\Web\Uri
     */
    public function setBase($path)
    {
        if (is_string($path)) {
            // Normalize alias
            $this->base = '/' . trim($path, '/');
        }
        return $this;
    }


    /**
     * Returns base alias
     *
     * @return null|string
     */
    public function getBase()
    {
        return $this->base;
    }


    /**
     * Sets uri query
     *
     * @param string $query
     * @param mixed $value [optional]
     *
     * @return \Smart\Web\Uri
     */
    public function setQuery($query, $value = null)
    {
        if (is_string($query)) {
            if (is_null($value)) {
                // Parse query
                parse_str($query, $this->query);
            } else {
                $this->query[$query] = $value;
            }
        }
        return $this;
    }


    /**
     * Returns uri query
     *
     * @param null|string $name [optional]
     * @param null|mixed $default [optional]
     *
     * @return null|mixed
     */
    public function getQuery($name = null, $default = null)
    {
        if (!is_string($name)) {
            return $this->query;
        }
        return isset($this->query[$name]) ? $this->query[$name] : $default;
    }


    /**
     * Sets uri fragment
     *
     * @param string $fragment
     *
     * @return \Smart\Web\Uri
     */
    public function setFragment($fragment)
    {
        if (is_string($fragment)) {
            $this->fragment = $fragment;
        }
        return $this;
    }


    /**
     * Returns uri fragment
     *
     * @return null|string
     */
    public function getFragment()
    {
        return $this->fragment;
    }


    /**
     * Sets uri scheme
     *
     * @param string $scheme
     * @return \Smart\Web\Uri
     */
    public function setScheme($scheme)
    {
        if (is_string($scheme)) {
            if (in_array($scheme, $this->schemes)) {
                $this->scheme = $scheme;
            }
        }

        return $this;
    }


    /**
     * Returns uri scheme
     *
     * @return string
     */
    public function getScheme()
    {
        return $this->scheme;
    }


    /**
     * Check uri for valid
     *
     * @return bool
     */
    public function valid()
    {
        $this->valid = true;

        if (!$this->validScheme()) {
            $this->valid = false;
        }

        if (!$this->validHost()) {
            $this->valid = false;
        }

        return $this->valid;
    }


    /**
     * Returns uri scheme valid state
     *
     * @param string $scheme [optional]
     *
     * @return bool
     */
    public function validScheme($scheme = null)
    {
        if (is_null($scheme)) {
            $scheme = $this->scheme;
        }
        if (is_null($scheme)) {
            return true;
        }
        return in_array($scheme, $this->schemes);
    }


    /**
     * Returns uri host valid state
     *
     * @param string $host [optional]
     *
     * @return bool
     */
    public function validHost($host = null)
    {
        if (is_null($host)) {
            $host = $this->host;
        }
        if (is_null($host)) {
            return true;
        }
        return (bool) preg_match('#^\[?(?:[' . self::ALNUM . '-:\]_]+\.?)+$#ui', $host);
    }


    /**
     * Returns uri alias valid state
     *
     * @param string $path [optional]
     *
     * @return bool
     */
    public function validPath($path = null)
    {
        if (is_null($path)) {
            $path = $this->path;
        }
        if (is_null($path)) {
            return true;
        }
        return (bool) preg_match('#^[' . self::ALNUM . '._\-/:]+$#ui', $path);
    }



    protected function formServer($array)
    {
        $this->uri = '/';
        if ($array['REQUEST_URI']) {
            $this->uri = trim($array['REQUEST_URI']);
        }

        if (!preg_match('#^(https?://|^/)#', $this->uri)) {
            $this->uri = $this->scheme . '://' . $this->uri;
        }

        $url = parse_url($this->uri);

        // Set hostname
        $this->setHost(isset($url['host']) ? $url['host'] : $_SERVER['SERVER_NAME']);
        // Set port
        $this->setPort(isset($url['port']) ? $url['port'] : $_SERVER['SERVER_PORT']);
        // Set alias
        $this->setPath(isset($url['path']) ? $url['path'] : null);
        // Set query
        $this->setQuery(isset($url['query']) ? $url['query'] : null);
        // Setup scheme
        $this->setScheme(isset($url['scheme']) ? $url['scheme'] : null);
        // Set fragment
        $this->setFragment(isset($url['fragment']) ? $url['fragment'] : null);


        // Validate uri
        $this->valid();

        return $this;

    }


    protected function formArray($array)
    {

    }


}