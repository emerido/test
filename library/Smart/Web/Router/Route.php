<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart\Web\Router;


class Route
{

    /**
     * Route http path
     *
     * @var string
     */
    protected $path;


    /**
     * Route http methods
     *
     * @var array
     */
    protected $verbs;


    /**
     * Route RegExp rules
     *
     * @var array
     */
    protected $rules = [
        'path'  => '.+',
        'part'  => '[^/]+',
        'str'   => '[\w]+',
        'int'   => '[\d]+',
    ];


    /**
     * Compiled route http path regular expression
     *
     * @var string
     */
    protected $regex;


    /**
     * Route order place
     *
     * @var int
     */
    protected $order = 0;


    /**
     * Route params
     *
     * @var array
     */
    protected $param = [];


    /**
     * Before route call filter
     *
     * @var array
     */
    protected $before = [];


    /**
     * Route callback
     *
     * @var callable
     */
    protected $callback;


    /**
     * After route call filter
     *
     * @var array
     */
    protected $after = [];


    /**
     * Instantiate route
     *
     * @param string $path
     * @param string|array $verb [optional]
     * @param callable|null $func [optional]
     */
    public function __construct($path, $verb = '*', $func = null)
    {
        $this->setPath($path);
        $this->setMethod($verb);

        if (!is_null($func)) {
            $this->setCallback($func);
        }
    }


    /**
     * Define supported http verbs
     *
     * @param string|array $verb
     *
     * @return \Smart\Web\Router\Route
     */
    public function via($verb)
    {
        return $this->setMethod(func_get_args());
    }
    
    
    /**
     * Check for route match for passed credentials
     *
     * @param string $path
     * @param string $verb
     *
     * @return bool
     */
    public function match($path, $verb)
    {
        // Skip if route not supports request method
        if ($this->hasMethod($verb) === false) {
            return false;
        }

        if ($this->regex === null) {
            $this->compile();
        }

        if (preg_match($this->regex, $path, $matches)) {
            foreach ($matches as $key => $value) {
                $this->setParam($key, $value);
            }
            return true;
        }
        return false;
    }


    /**
     * Compile route path pattern
     *
     * @return \Smart\Web\Router\Route
     */
    public function compile()
    {
        // Handle last slash
        $path = $this->path . (substr($this->path, -1) === '/' ? '?' : '/?');
        $rule = $this->rules;

        // Create regular expression
        $this->regex = preg_replace_callback('#\{([\w\d]+)(?::([\w\d]+))?\}?#', function ($match) use ($rule) {
            if (false === isset($match[2]))
                $match[2] = 'part';
            if (true === isset($rule[$match[2]]))
                $match[2] = $rule[$match[2]];
            return '(?P<' . $match[1] . '>' . $match[2] . ')';
        }, strtr($path, array(')' => ')?')));

        // Pick complete pattern
        $this->regex = '#^' . $this->regex . '$#ui';

        return $this;
    }


    /**
     * Adds "before" filter for route
     *
     * @param callable $func
     *
     * @throws \Exception
     *
     * @return \Smart\Web\Router\Route
     */
    public function setBefore($func)
    {
        if (!is_callable($func)) {
            throw new \Exception('Before filter must be a callable. ' . gettype($func) . ' given.');
        }
        if (!in_array($func, $this->before)) {
            $this->before[] = $func;
        }
        return $this;
    }


    /**
     * Returns route "before" callbacks
     *
     * @return array
     */
    public function getBefore()
    {
        return $this->before;
    }


    /**
     * Adds "after" filter for route
     *
     * @param callable $func
     *
     * @throws \Exception
     *
     * @return \Smart\Web\Router\Route
     */
    public function setAfter($func)
    {
        if (!is_callable($func)) {
            throw new \Exception('After filter must be a callable. ' . gettype($func) . ' given.');
        }
        if (!in_array($func, $this->after)) {
            $this->after[] = $func;
        }
        return $this;
    }


    /**
     * Returns route "after" callbacks
     *
     * @return array
     */
    public function getAfter()
    {
        return $this->after;
    }


    /**
     * Sets route callback
     *
     * @param callable $func
     *
     * @throws \Exception
     *
     * @return \Smart\Web\Router\Route
     */
    public function setCallback($func)
    {
        if (!is_callable($func)) {
            throw new \Exception('Route callback must be a callable');
        }
        return $this->callback = $func;
    }


    /**
     * Returns route callback
     *
     * @return callable
     */
    public function getCallback()
    {
        return $this->callback;
    }


    /**
     * Sets route path
     *
     * @param string $path
     *
     * @return \Smart\Web\Router\Route
     */
    public function setPath($path)
    {
        if (is_string($path)) {
            $this->path = $path;
        }
        return $this;
    }


    /**
     * Returns path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * Sets route order
     *
     * @param int $int
     *
     * @return \Smart\Web\Router\Route
     */
    public function setOrder($int)
    {
        if (is_int($int)) {
            $this->order = $int;
        }
        return $this;
    }


    /**
     * Returns route order
     *
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }


    /**
     * Sets route http methods
     *
     * @param string... $method
     *
     * @return \Smart\Web\Router\Route
     */
    public function setMethod($method)
    {
        $this->verbs = [];
        $self = $this;
        array_map(function($method) use ($self) {
            if (is_array($method)) {
                call_user_func_array([$this, 'setMethod'], $method);
            } elseif (is_string($method) and !in_array($method, $this->verbs)) {
                $this->verbs[] = strtoupper($method);
            }
        }, func_get_args());

        return $this;
    }


    /**
     * Returns route http methods
     *
     * @return array
     */
    public function getMethod()
    {
        return $this->verbs;
    }


    /**
     * Returns true if route supports http method
     *
     * @param string $verb
     *
     * @return bool
     */
    public function hasMethod($verb)
    {
        if (is_string($verb)) {
            if (in_array('*', $this->verbs)) {
                return true;
            } else {
                return in_array(strtoupper($verb), $this->verbs);
            }
        }
        return false;
    }


    /**
     * Sets route regexp rule
     *
     * @param string $name
     * @param string $rule
     *
     * @return \Smart\Web\Router\Route
     */
    public function setRule($name, $rule)
    {
        if (is_string($name) and is_string($rule)) {
            $this->rules[trim($name)] = $rule;
        }
        return $this;
    }


    /**
     * Returns rule(s)
     *
     * @param string|null $name
     *
     * @return array|string
     */
    public function getRule($name = null)
    {
        if (is_string($name) and isset($this->rules[$name])) {
            return $this->rules[$name];
        }
        return $this->rules;
    }


    /**
     * Sets route param
     *
     * @param string $name
     * @param mixed $value
     *
     * @return \Smart\Web\Router\Route
     */
    public function setParam($name, $value)
    {
        if (is_string($name)) {
            $this->param[$name] = $value;
        }
        return $this;
    }


    /**
     * Returns param(s)
     *
     * @param string|null $name
     *
     * @return array|mixed
     */
    public function getParam($name = null)
    {
        if (is_string($name) and isset($this->param[$name])) {
            return $this->param[$name];
        }
        return $this->param;
    }


}