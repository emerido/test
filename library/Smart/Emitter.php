<?php
/**
 * Smart - PHP 5 Content Management Framework
 *
 * @author      Shvorak Alexey <dr.emerido@gmail.com>
 * @copyright   2013 Shvorak Alexey
 * @link        http://www.smartframework.com
 * @license     http://www.smartframework.com/license
 * @version     0.0.1
 * @package     Smart
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Smart;


class Emitter
{


    /**
     * Emitter listeners
     *
     * @var array
     */
    protected $listeners = [];


    /**
     * Emits event listeners
     *
     * @param string $name
     * @param mixed $args
     */
    public function emit($name, $args = [])
    {
        if (is_string($name) and isset($this->listeners[$name])) {
            $args = (array) $args;
            array_map(function ($callback) use ($args) {
                call_user_func_array($callback, $args);
            }, $this->listeners[$name]);
        }
    }


    /**
     * Adds event listener
     *
     * @param string $name
     * @param callable $callback
     *
     * @throws \Exception
     */
    public function on($name, $callback)
    {
        if (is_string($name)) {
            // Make array
            if (isset($this->listeners[$name]) === false) $this->listeners[$name] = [];
            // Save callback
            if (is_callable($callback)) {
                $this->listeners[$name][] = $callback;
            } else {
                throw new \Exception('Event callback must be a callable. ' . gettype($callback) . ' given');
            }
        }
    }

}